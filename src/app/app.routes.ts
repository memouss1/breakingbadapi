import { Routes } from "@angular/router";
import { ComponenteUnoComponent } from "./components/componente-uno/componente-uno.component";
import { HomeComponent } from "./components/home/home.component";


export const ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'componente/uno', component: ComponenteUnoComponent },
    { path: '', pathMatch: 'full', redirectTo: 'home' },
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

