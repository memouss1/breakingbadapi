import { Component, OnInit } from '@angular/core';
import { BreakingbadService } from 'src/app/services/brakingbad/breakingbad.service';


@Component({
  selector: 'app-componente-uno',
  templateUrl: './componente-uno.component.html',
  styleUrls: ['./componente-uno.component.css']
})
export class ComponenteUnoComponent implements OnInit {

  public personajes : any[] = [];

  constructor( private _breackingBad : BreakingbadService ) { }

  ngOnInit(): void {

    this.getTodosPersonajes();

  }

  getTodosPersonajes(){

    this.personajes = [];

    this._breackingBad.getPersonajes()
    .subscribe((data: any) => {
      console.log(data);

      this.personajes = data;

      console.log('Personajes SETEADOS',this.personajes);
      
      
    })
  }

}
