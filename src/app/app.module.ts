import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ComponenteUnoComponent } from './components/componente-uno/componente-uno.component';

//! Invocar el RouterModule
import { RouterModule } from '@angular/router';
//! Invocar el AppRoutes
import { ROUTES } from './app.routes';
import { NavBarComponent } from './components/share/nav-bar/nav-bar.component';

import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ComponenteUnoComponent,
    NavBarComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES, {useHash: true})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
