import { Injectable } from '@angular/core';
import { HttpClient  } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BreakingbadService {

  //? Escribir URL a la cual se hará la petición.
  public URL = 'https://www.breakingbadapi.com/api';

  //? Inyectar el método HTTPCLIENT en el constructor 
  constructor( private _http : HttpClient ) { }

  //! Escribir mi petición

  getPersonajes() {

    const uri = `${this.URL}/characters`;

    return this._http.get(uri);
  }

}
